// set up dependencies
const User = require ("../models/user.js")
const Course = require ("../models/course.js")
const auth = require ("../auth.js")
const bcrypt = require ("bcrypt") // used to encrypt user passwords

// check if the email exists
/*
	1. check for the email
	2. send the result as a response (with error handling)
*/

// it is conventional for the devs to use Boolean in sending return response esp with the backend application
module.exports.checkEmail = (requestBody) => {
	return User.find ({email: requestBody.email}).then ((result, error) => {
		if (error){
			console.log(error)
			return false
		} else {
			if (result.length > 0){
				//  return result
				return true
			} else {
				// return res.send ("email does not exist")
				return false
			}
		}
	})
}

/*
USER REGISTRATION

1. create a new User with the information from the requestBody
2. make sure that the password is encrypted
3. save the new user
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age:reqBody.age,
		gender:reqBody.gender,
		email: reqBody.email,
		// hashSync is a function of bcrypt that encrypts the password
			// 10 is the number of rounds/times it runds the algorithm to the reqBody.password
				// max 72 implementations
		password:bcrypt.hashSync(reqBody.password, 10),
		mobileNumber:reqBody.mobileNumber
	})
	return newUser.save().then((saved,error) => {
		if (error){
			console.log(error)
			return false
		} else {
			return true
		}
	})
}

// USER LOGIN
/*
1. find if the email is existing in the database
2. check if the password is correct
*/

module.exports.userLogin = (reqBody) => {
	return User.findOne({email:reqBody.email}).then (result => {
		if (result === null){
			return false
		} else {
			// compareSync function - used to compate a non-encrypted password to an encrypted password and returns a Boolean response depending on the result
			bcrypt.compareSync(reqBody.password, result.password)

			/*
			What should we do after the comparison?
			true - a token should be created since the user is existing and the password is correct
			false - the passwords do not match, thus a token should not be created 
			*/
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
			// auth - imported auth.js
			// createAccessToken - function inside the auth.js to create access token
			// .toObject - transforms the User into an Object that can be used by our createAccessToken
			} else {
				return false
			}
		}
	})
}

/*
mini activity
	create getProfile function inside the userController
		1. find the id of the user using "data" as parameters
		2. if it does not exist, return false
		3. otherwise, return the details (stretch - make the password invisible)
*/

// getProfile
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then (result => {
		if (result === null) {
			return false
		} 
		else {
			result.password = ""
			return result
		}
	})
}

/*
1. find the user/document in the database
2. add the courseId to the user's enrollment array
*/

// enrollment
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// adding the courseId to the user's enrollment array
		user.enrollments.push({courseId:data.courseId})

		return user.save().then ((user,err) =>{
			if (err){
				return false
			} else {
				return true
			}
		})
	})

	if (isUserUpdated) {
		return true
	}
	else {
		return false
	}
}


/*
use another await keyword to update the enrollees array in the course collection
1. find the courseId in the requestBody
2. push the userId of the enrolle in the enollees array of the course
3. update the document in the database

if both pushing are successful, return true
if both pushing are successful, return false
*/


// enrollees
module.exports.enrolleeslist = async (data2) => {
	let isCourseUpdated = await User.findById(data2.userId).then(course => {
		// adding the courseId to the user's enrollment array
		course.enrollees.push({userId:data.userId})

		return user.save().then ((course,err) =>{
			if (err){
				return false
			} else {
				return true
			}
		})
	})

	if (isCourseUpdated) {
		return true
	}
	else {
		return false
	}
}