const User = require ("../models/user.js")
const Course = require ("../models/course.js")
const auth = require ("../auth.js")
const bcrypt = require ("bcrypt") 


/*
ACTIVITY s34
	1. find the user in the database
		find out if the user is admin

	2. if the user is not an admin, send the response "You are not an admin"

	3. if the user is an admin, create the new Course object
			-save the object
				-if there are errors, return false
				-if there are no errors, return "Course created sucessfully"*/



module.exports.addCourse = (reqBody, userData) => {
	
	return User.findById(userData.userId).then (result => {
		if (userData.isAdmin == false) {
			return "You are not an admin"
		}
		else {
			let newCourse = new Course ({
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price,				
			})
			// 
			return newCourse.save().then ((course,error) => {
				if(error){
					// 
					return false
				} else {
					// Course creation sucessful
					return "Course creation sucessful"
				}
			})
		}
	});
}

module.exports.getAllCourses = () => {
	return Course.find ({}).then (result =>{
		return result
	})
}

/*
using the course id in the url, retrieve a course using a get request
*/

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then (result => {
		return result
	})
}

// retrieve all active courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then ((result,error) => {
		if (error) {
			console.log(error)
		} else {
			return result
		}
	})
}


// update a course
module.exports.updateCourse = ( reqParams, reqBody ) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate - looks for the id of the document (first parameter) and updates it (content of the second parameter)
		//the server will be looking for the id of the reqParams.courseId in the database and updates the document through the content of the object updatedCourse 
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

// archive a course
/*
controllers business logic: 

    1. create a updateCourse object with the content from the reqBody
            reqBody should have the isActive status of the course to be set to false
    2. find the course by its id and update with the updateCourse object using the findByIdAndUpdate
            handle the errors that may arise
                error/s - false
                no errors - true
*/


// archive a course
module.exports.archiveCourse = ( reqParams, reqBody ) => {
	let archivedCourse = {
		isActive: reqBody.isActive,
	}
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

/*
if you do want to requre any resuest body:
module.exports.archiveCourse = ( reqParams ) => {
	let archivedCourse = {
		isActive: false
}
*/