const mongoose = require ("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name is required"]
	},
	description: {
		type: String,
		required: [true, "Course description is required"]
	},
/*
	create a similar data structure for price (number), isActive (Boolean), createdOn(type: date, default value: new Date())
*/
	price:{
		type: Number,
		required: [true, "Price for the course is required"]
		},

	isActive: {
		type: Boolean,
		default: true
	},
	createOn: {
		type: Date,
		default: new Date()
	},
	/*
		enrolees:
		{userId: qwijresa1242334
		enrolledOn: Apr.26 2022},
		{}
		{}
		{}
		
	*/
	enrollees:[
	{
		userId:{
			type: String,
			required: [true, "User ID is required"]
		},
		enrolledOn: {
			type: Date,
			default: new Date()
		}
	}]			
})

module.exports = mongoose.model("Course", courseSchema)
