/*
	user schema

	firstName - string
	lastName - string
	age - string
	gender- string
	email - string
	password - string
	mobileNumber - string

	isAdmin - Boolean, false

	enrollement array - will show the courses(id) where the students are enrolled

	push the updates on your s32-36 git repo with the commit message "add user schema"
	add it on Boodle
*/

const mongoose = require ("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	age: {
		type: String,
		required: [true, "Age is required"]
	},
	gender: {
		type: String,
		required: [true, "Gender is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile number is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	enrollments:[
	{
		courseId:{
			type: String,
			required: [true, "Course ID is required"]
		},
		enrolledOn: {
			type: Date,
			default: new Date()
		},
		// to see if the students is still enrolled or not (graduated/dropped) in the course
		status: {
			type: String,
			default: "Enrolled"
		}
	}]			
})

module.exports = mongoose.model("User", userSchema)