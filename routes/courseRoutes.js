const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController.js")


/*
create a route that will let an admin perform addCourse function in the courseController
	verify that the user is logged in
	decode the token for that user
	use the id, isAdmin, and request body to perform the function in courseController
		the id and isAdmin are parts of an object 
*/


router.post ("/", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization) //userData wound now contain an object that has token payload (id, emai, isAdmin information of the user)
	courseController.addCourse(req.body, userData).then(resultFromController => res.send (resultFromController))
})

// ecommerce websites

/*
create a route that will retrieve all of our products/courses
	will require login/register functions
*/

router.get("/", (req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})


// getting all of the documents, in case we need multiple of them, place the route with the criteria ot the find method first
router.get("/active", (req,res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})

/*
mini activity
	create a route that will retrieve a course
*/
router.get("/:courseId", (req,res) => {
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(result => res.send(result))
})

// update a course
router.put("/:courseId", auth.verify, (req,res) => {
	courseController.updateCourse(req.params, req.body).then(result => res.send(result))
})

/*
delete is never a norm in database

use/archiveCourse and send a PUT request to archive course by changing the active status
*/


// archive course
router.put("/:courseId/archive", auth.verify, (req,res) => {
	courseController.archiveCourse(req.params, req.body).then(result => res.send(result))
})


module.exports = router